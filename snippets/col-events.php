<div class="col events-col <?php the_sub_field('event_width'); ?>">
	<ul>
		<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
			<?php dynamic_sidebar( 'events-sidebar' ); ?>
		<?php else : endif; ?>
	</ul>
</div>