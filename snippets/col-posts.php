<div class="col news-col <?php the_sub_field('posts_width'); ?>">
	<h3><?php the_sub_field('posts_title'); ?></h3>
	<?php $term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		$posts_query = new WP_Query( array( 'showposts' => $amount, 'cat' => $term->term_id ) ); ?>
	<ul>
		<?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
		<a href="<?php the_permalink() ?>">
			<li>
				<?php 
					if(get_sub_field('posts_width') == "two") {
						if(get_sub_field('show_image') == "yes") {
							if ( has_post_thumbnail() ) {
								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
								$url = $thumb['0']; ?>
								<img src="<?=$url?>" alt="<?php the_title(); ?>" />
							<?php } else { ?>
						        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
						<?php }
						}
					} 
				?>
                
                <?php 
					if(get_sub_field('posts_width') == "one") {
						if(get_sub_field('show_image') == "yes") {
							if ( has_post_thumbnail() ) {
								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-medium-thumb' );
								$url = $thumb['0']; ?>
								<img src="<?=$url?>" alt="<?php the_title(); ?>" />
							<?php } else { ?>
						        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
						<?php }
						}
					} 
				?>
				<div class="news-item">
					<h4><?php the_title(); ?></h4>
					<p>
						<?php $content = get_the_content();
						$trimmed_content = wp_trim_words( $content, 27, '...' );
						echo $trimmed_content; ?>
					</p>
				</div>
			</li>
		</a>
		<?php endwhile; ?>
	</ul>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<a class="btn" href="/category/<?php echo $term->slug; ?>/">View All<span class="hidden"> <?php echo $term->name; ?></span></a>
</div>