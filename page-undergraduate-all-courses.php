<?php
/*
 Template Name: All Undergraduate Courses
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
						<section>
							<?php // Courses ?>
							<?php $courses_loop = new WP_Query(
								array('post_type' => 'courses', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' => 
								array(
									array(
										'key' => 'program',
										'value' => 'undergraduate',
										'compare' => 'LIKE'
									))
								));
							?>
							<?php if ($courses_loop->have_posts()) : while ($courses_loop->have_posts()) : $courses_loop->the_post(); ?>
                            <h3><?php if(get_field('course_link')) { ?><a href="<?php the_field('course_link'); ?>"><?php } ?>
								<?php the_title(); ?>
								<?php if(get_field('course_link')) { ?></a><?php } ?>
							</h3>
                            <?php if(get_field('course_number')): ?>
                                <h4><?php the_field('course_number'); ?></h4>
                            <?php endif; ?>
							<?php if(get_field('instructor_type') == "internal") { ?>
							<span class="instructors">
								<strong>Instructor: </strong>
								<?php $instructor = get_field('instructor'); ?>
								<?php if($instructor): $post = $instructor; setup_postdata($post); ?>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<?php $courses_loop->reset_postdata(); ?>
								<?php endif; ?>
							</span>
							<?php }	?>
							<?php if(get_field('instructor_type') == "external") { ?>
							<span class="instructors">
								<?php if(get_field('additional_instructors')) { ?>
								<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
								<?php } ?>
							</span>
							<?php }	?>
							<?php if(get_field('instructor_type') == "both") { ?>
							<span class="instructors">
								<strong>Instructor: </strong>
								<?php $instructor = get_field('instructor'); ?>
								<?php if($instructor): $post = $instructor; setup_postdata($post); ?>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $courses_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
							</span>
							<?php }	?>
							<?php the_content(); ?>
							<?php endwhile; else : ?>
							<p>There are no undergraduate courses this quarter.</p>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
						</section>
                        <?php if(get_field('past_courses')) { ?>
                            <section class="past-courses">
                                <h2>Past Courses</h2>
                                    <?php the_field('past_courses'); ?>
                            </section>
                        <?php } ?>
					</article>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>