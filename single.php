<?php get_header(); ?>
			<div class="content">
				<div class="col main" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_content(); ?>
						</section>
					</article>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry cf">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
							<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

				<?php endif; ?>
				
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
