<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">
					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
