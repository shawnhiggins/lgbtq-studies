<?php get_header(); ?>
			<div class="content main">
				<div class="col">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<header>
							<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
							<?php if(get_field('subtitle')) { ?>
							<h2 class="subtitle"><?php the_field('subtitle'); ?></h2>
							<?php } ?>
							<?php if(get_field('publisher')) { ?>
							<span class="publisher"><?php the_field('publisher'); ?>, <?php the_field('published_date'); ?></span>
							<?php } ?>
							<span class="author"><strong>Author(s): </strong>
								<?php $author = get_field('author'); ?>
								<? if( $author ): ?>
								<?php foreach( $author as $post): ?>
								<?php setup_postdata($post); ?>
								<a href="<?php the_permalink(); ?>" class="author-name"><?php the_title(); ?></a><?php endforeach; ?><?php wp_reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_authors')) { ?>, <?php the_field('additional_authors'); ?>
								<?php } ?>
							</span>
							<?php if(get_field('book_cover')) {
								$image = get_field('book_cover');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'large-book';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
								<?php } else { ?>
								<div class="custom-cover cover">
									<span class="title"><?php the_title(); ?></span>
								</div>
							<?php } ?>
						</header>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_content(); ?>
						</section>
					</article>
					<?php endwhile; ?>
					<?php else : ?>
						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>
					<?php endif; ?>
					</div>
					<div class="col">
						<?php if(get_field('book_cover')) {
							$image = get_field('book_cover');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'large-book';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
							<?php } else { ?>
							<div class="custom-cover cover">
								<span class="title"><?php the_title(); ?></span>
							</div>
						<?php } ?>	
					</div>
				</div>
<?php get_footer(); ?>