<?php get_header(); 
// vars
            $queried_object = get_queried_object(); 
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $term_slug = $queried_object->slug;
             //echo $term_slug;
?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (is_category()) { ?>
					<h1 class="archive-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } ?>
                    <div class="<?php echo $term_slug; ?>-list">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title">
                            <!--// a href="<?php the_permalink() ?>" rel="bookmark" //-->                            
                                <?php the_title(); ?>                                
                            <!--// /a //-->
                        </h3>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php if (($term_slug == 'lecture-series') || ($term_slug == 'conferences')){
                                   
                                }  else {
                                        the_excerpt(); 
                                        }
                            ?>
				                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
                        </article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>
                    </div>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>