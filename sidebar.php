				<?php // For posts
				if (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>


<!--/*

			'events-nav' => __( 'Events', 'bonestheme' ), // Events

*-->
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'graduate-nav',
										'theme_location' => 'graduate-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Student Org subpage
								if (is_tree(1343) || get_field('menu_select') == "student-org") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Student Organizations', 'bonestheme' ),
									   	'menu_class' => 'student-org-nav',
									   	'theme_location' => 'student-org-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Student Organizations</h3> <ul>%3$s</ul>'
									));
								}
                                // If an Events subpage
								if (is_tree(1974) ||is_category('Lecture Series') || get_field('menu_select') == "events") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Events', 'bonestheme' ),
									   	'menu_class' => 'events-nav',
									   	'theme_location' => 'events-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Events</h3> <ul>%3$s</ul>'
									));
								}
								// If an Photos subpage
								if (is_tree(1345) || get_field('menu_select') == "photos") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Photos', 'bonestheme' ),
									   	'menu_class' => 'photos-nav',
									   	'theme_location' => 'photos-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Photos</h3> <ul>%3$s</ul>'
									));
								}
								// If an Giving subpage
								if (is_tree(1347) || get_field('menu_select') == "giving") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Giving', 'bonestheme' ),
									   	'menu_class' => 'giving-nav',
									   	'theme_location' => 'giving-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Giving</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>